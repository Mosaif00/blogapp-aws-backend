const router = require('express').Router();
const User = require('../models/User');
const bcrypt = require('bcrypt');

// Update User 
router.put('/:id', async (req, res) => {
    console.log(req.body)
    console.log(req.params.id)
    if (req.body.userId === req.params.id) {
        console.log('test')
        if (req.body.password) {
            const salt = await bcrypt.genSalt(10);
            req.body.password = await bcrypt.hash(req.body.password, salt);
        }
        try {
            const updateUser = await User.findByIdAndUpdate(req.params.id, {
                $set: req.body
            }, { new: true });
            console.log(User.findByIdAndUpdate(req.params.id))

            console.log(updateUser)
            res.status(200).json(updateUser);

        } catch (error) {
            return res.status(500).json({ message: 'something went wrong updating account', error });
        }
    } else {
        return res.status(401).json('You are not allowed to update this account');
    }

});
// Delete User 
router.delete('/:id', async (req, res) => {
    if (req.body.userId !== req.params.id) {

        try {
            const deleteUser = await User.findByIdAndDelete(req.params.id, {
                $set: req.body
            }, { new: true });

            res.status(200).json({ message: 'user is deleted', deleteUser });

        } catch (error) {
            return res.status(500).json({ message: 'something went wrong deleting account', error });
        }
    } else {
        return res.status(401).json('You are not allowed to delete this account');
    }

});
// Get User 
router.get('/:id', async (req, res) => {

    try {
        const findUser = await User.findById(req.params.id);

    
        const {password,...others}=findUser._doc


        res.status(200).json(others);

    } catch (error) {
        return res.status(500).json({ message: 'something went wrong getting user', error });
    }


});


module.exports = router;