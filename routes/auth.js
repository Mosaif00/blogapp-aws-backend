const router = require('express').Router();
const bcrypt = require('bcrypt');
const User = require('../models/User');

// Register 
router.post('/register', async (req, res) => {
    try {
        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(req.body.password, salt);
        const newUser = new User({
            username: req.body.username,
            email: req.body.email,
            password: hashPassword,
        });

        const user = await newUser.save();

        return res.status(200).json(user);

    } catch (error) {
        return res.status(500).json({ message: 'something went wrong registering', error });
    }
});



// Login
router.post('/login', async (req, res) => {
    try {
        const user = await User.findOne({
            email: req.body.email
        });


        if (!user) {
            res.status(400).json('User not found');
        }

        const validateUser = await bcrypt.compare(req.body.password, user.password);

        if (!validateUser) {
            res.status(400).json('Wrong password');

        }

        const { password, ...others} = user._doc
        res.status(200).json(others);


    } catch (err) {
        res.status(500).json(err);

    }
});


module.exports = router;