const router = require('express').Router();
const Post = require('../models/Post');

// Get all post 
router.get('/', async (req, res) => {
    const username = req.query.user;
    const category = req.query.category;
    try {
        let posts; 
        if(username){
            posts = await Post.find({username })
            console.log('test')
        } else if(category){
            posts = await Post.find({categories: {
                $in:[category]
            }})
            console.log('test1')

        }else {
        posts = await Post.find()

        }


        return res.status(200).json(posts);
    } catch (error) {
        return res.status(500).json({ message: 'something went wrong getting all posts', error });
    }


});

// Create Post 
router.post('/', async (req, res) => {
    const newPost = new Post(req.body);
    try {
        const savedPost = await newPost.save();

        return res.status(200).json(savedPost);
    } catch (error) {
        return res.status(500).json({ message: 'something went wrong creating post', error });
    }


});

//  Update Post
router.put('/:id', async (req, res) => {

    try {
        const post =await Post.findById(req.params.id);
       
        if (post.username !== req.body.username) {
          
            return res.status(401).json('You are not allowed to update this post');
          
        }

        const updatePost = await Post.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, { new: true });
        return res.status(200).json({ message: 'post is updated', updatePost });


    } catch (error) {
        return res.status(500).json({ message: 'something went wrong updating post', error });
    }


});
// Delete Post
router.delete('/:id', async (req, res) => {

    try {
        const post =await Post.findById(req.params.id);
       
        if (post.username !== req.body.username) {
          
            return res.status(401).json('You are not allowed to remove this post');
          
        }


        await post.delete()

        res.status(200).json('post deleted');

    } catch (error) {
        return res.status(500).json({ message: 'something went wrong deleting post', error });
    }


});

// Get Post
router.get('/:id', async (req, res) => {

    try {
        const post = await Post.findById(req.params.id)


        res.status(200).json(post);

    } catch (error) {
        return res.status(500).json({ message: 'something went wrong getting posts', error });
    }


});


module.exports = router;