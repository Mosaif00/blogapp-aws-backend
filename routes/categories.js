const router = require('express').Router();
const Category = require('../models/Category');



// Create Categories
router.post('/', async (req, res) => {
    const newCategory = new Category(req.body)
    try {
        const category = await newCategory.save()


        res.status(200).json(category);

    } catch (error) {
        return res.status(500).json({ message: 'something went wrong creating category', error });
    }


});

// Get All Categories
router.get('/', async (req, res) => {
    try {
        const categories = await Category.find()


        res.status(200).json(categories);

    } catch (error) {
        return res.status(500).json({ message: 'something went wrong creating category', error });
    }


});


module.exports = router;